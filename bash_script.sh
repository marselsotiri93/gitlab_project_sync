#!/bin/bash

source_repo_url="https://github.com/marselsotiri/test1.git"
target_repo_url="git@gitlab.com:marselsotiri93/gitlab_project_sda.git"

# Directory to store the local copies of repositories
source_repo_dir="source_repo"
target_repo_dir="target_repo"

# Clone or update the source repository
if [ -d "$source_repo_dir" ]; then
  cd "$source_repo_dir"
  git fetch origin
  git reset --hard origin/main
  cd ..
else
  git clone "$source_repo_url" "$source_repo_dir"
fi

# Clone or update the target repository
if [ -d "$target_repo_dir" ]; then
  cd "$target_repo_dir"
  git fetch origin
  git reset --hard origin/main
  cd ..
else
  git clone "$target_repo_url" "$target_repo_dir"
fi

# Check if there are any new changes in the source repository
cd "$source_repo_dir"
source_head=$(git rev-parse HEAD)
cd ..

cd "$target_repo_dir"
target_head=$(git rev-parse HEAD)
cd ..

if [ "$source_head" != "$target_head" ]; then
  # Synchronize the target repository with the source repository
  cd "$source_repo_dir"
  git remote add target "$target_repo_url"
  git push target --force --all
  git push target --force --tags
  cd ..

  echo "Changes synchronized."
else
  echo "No new changes to synchronize."
fi
